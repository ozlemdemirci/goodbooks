<?php

    interface CarInterface
    {
        public function goForwardBack();
        public function turnRightLeft();
    }