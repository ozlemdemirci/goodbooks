<?php

class Mercedes implements CarInterface
{
    function __construct($model)
    {
        $this->model = $model;
    }

    function goForwardBack()
    {
        echo "$this->model can go forward and back! <br>";
    }

    public function turnRightLeft()
    {
        echo "$this->model can turn right and left! <br>";
    }
}