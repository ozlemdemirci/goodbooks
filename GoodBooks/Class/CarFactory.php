<?php

    include 'Mercedes.php';
    class CarFactory
    {
        function __construct($model)
        {
            switch ($model){
                case 'Mercedes':
                    $mercedes = new Mercedes($model);
                    $mercedes->goForwardBack();
                    $mercedes->turnRightLeft();
                    break;

                default:
                    echo 'Undefined Model';
            }
        }
    }